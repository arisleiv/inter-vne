# README #

Simulator for Controlling Virtual Infrastructures (CVI – Sim)

1	Scope
Given a set of Virtual Networks (VN) requested by users, the simulator must assign virtual resources to the users over a set of substrate networks (SN) using different resource allocation and optimization algorithms. Once a simulation has finished, the tool can show statistics about the utilization of the substrate resources along with Virtual Network Embedding performance metrics such as request acceptance ratio, embedding cost, revenue etc. The performance metrics will show how each of the defined algorithms work in the long run. To run the GUI the user should select from the package main the SimulatorMain class.
 
2	Requirements

2.1	Substrate
2.1.1	Substrate elements and parameters
Substrate elements:
1.	Servers
2.	Routers
3.	Links
Parameters needed to perform Resource Discovery:
Functional Attributes:
-	Operating System (Linux, Windows, Solaris etc) [2]
-	Virtualization Environment (e.g. XEN, VMware, KVM etc.) [2]
-	Node Type (Server, Router etc.) [2]
-	Network Stack ( i.e. IP, ATM, Eth) [2]
-	Interface Type (Ethernet, Optical , radio etc) [2]
-	Location
Parameters used for the SN elements are theoretical ranges that could be mapped to real values. It is done this way in order to simplify any other work beyond the simulator objectives.
Non-functional Attributes:
-	Link utilization (bandwidth):
o	Value range: 50-100
-	Latency (determined by utilization)
-	CPU:
o	Value range: 50-100
-	Memory:
o	Value range: 50-100
-	Server Disk Space:
o	Value range: 50-100
-	Maximum Logical Instances in a Router:
o	Value range: 1-15
-	Layer2 VLANs
o	Value range: 0-4096
-	Number of active interfaces in the node
2.1.2	Substrate generation
The main option for creating Substrates (elements and topology) will be to design graphically in the simulator using the jung library. 
-	The substrate generation is based in the ErdosRenyiGenerator class of the jung library. ErdosRenyigenerator generates a random graph using the Erdos-Renyi binomial model (each pair of vertices is connected with probability p).
Another option could be to describe the substrate as an RSpec.

2.2	Request
An input of the simulator will be a Virtual Network (VN). Input will be introduced at the beginning of each simulation.
2.2.1	Request elements and parameters
Request elements needed for the simulator:
1.	Virtual Machines 
2.	Virtual Routers 
3.	Virtual Links
Functional Attributes:
-	Operating System (Linux, Windows, Solaris etc) [2]
-	Virtualization Environment (e.g. XEN, VMware, KVM) [2]
-	Virtual Node Type (VHost, VRouter) [2]
-	Network Stack ( i.e. IP, ATM, Eth) [2]
-	Interface Type (Ethernet, Optical , radio etc) [2]
Non - Functional Attributes:
At present, ranges shown below are fixed by the simulation in order to create random requests. Another option is to permit the modification of the ranges to the user. 
-	CPU:
o	Value range: 0-20
-	Memory
o	Value range: 0-20
-	VM Disk space
o	Value range: 0-20
-	Link Bandwidth
o	Value range 0-50
-	Number of interfaces for each node 
Other:
-	Arrival time of the VN request [3]
-	Period of time of reservation of the slice (lifetime)
-	Path splitting/unsplittable flows or both [1]

2.2.2	Request generation
Main option:
Generated Randomly:
In this section are listed the current parameters needed for a random generation of requests in the simulator.
Similar to the substrate generation the main option for creating Requests is through jung library. 
-	The request generation is based in the ErdosRenyiGenerator class of the jung library. ErdosRenyigenerator generates a random graph using the Erdos-Renyi binomial model (each pair of vertices is connected with probability p).
Another option could be to describe the substrate as an RSpec.
Inter-arrival process:  Poisson distribution (or any other appropriate e.g. Uniform, Normal etc.) to model the dynamic arrival of VN requests and define the arrival rate and the average lifetime. [4]
Parameters:
-	Prefix name
-	Number of requests
-	Range of min-max Virtual Nodes (uniform randomness)
-	Link probability
-	Start time distributions (arrivals of VN requests):
o	At once at time unit X
o	Uniform distribution
o	Normal distribution
o	Poisson distribution
-	Lifetime range (uniform randomness, exponential etc). 

2.3	Algorithms
CVI-Sim provides an assortment of algorithm to solve both the inter and intra Virtual Network Embedding problem.
Inter-domain Topology Embedding
-	Iterated Local Search (ILS) [5]
-	Exact [6]
Intra-domain Topology Embedding
-	Greedy Shortest Path (G-SP) [4]
-	Greedy Multi Commodity Flow (G-MCF) [7]
-	Networked Cloud Mapping (NCM) [8]
General considerations for Resource Allocation algorithms:
-	How to establish priorities in order to attend requests.
o	First-come-first-serve (on demand). Requests are allocated by the time they are issued.
-	Integrate Linear programming solvers e.g. 
o	IBM ILOG CPLEX [9]
o	GLPK (GNU Linear Programming Kit) [10]
2.4	Output
Statistics results obtained from the simulation: 
Results that can be consulted at the end of the simulation:
-	Substrate:
o	Global status (resources utilization e.g. cpu, memory, disk, router, link etc.)
o	Node status (yellow, red colour according to how stressed is the node)
o	Link status (yellow, red colour according to how stressed is the link)
o	substrate connectivity [4]
-	User request statistics:
o	Acceptance Ratio: The percentage of VN requests successfully embedded over the total amount of submitted requests.
o	Embedding Cost: The cost of embedding a VN request equals the amount of required bandwidth for all virtual links over the substrate paths as defined by the mapping solution, plus the amount of physical resources allocated to each virtual node associated with its type (e.g. memory, disk space, etc. for virtual machines).
o	Embedding Revenue: The embedding revenue of a VN request equals the amount of required bandwidth for all virtual links plus the requested amount of resources utilized by each virtual node associated with its type (e.g. memory, disk space, etc., for virtual machines). [1]
o	Hop Count: Average hop number per virtual link
 
 Implementation technologies
2.5	JUNG2 2.0.1
The JUNG (Java Universal Network/Graph) Framework is a free, open-source software library that provides a common and extendible language for the manipulation, analysis, and visualization of data that can be represented as a graph or network [11]. It is written in the Java programming language, allowing JUNG-based applications to make use of the extensive built-in capabilities of the Java Application Programming Interface (API), as well as those of other existing third-party Java libraries.
The major features of JUNG include the following:
-	Support for a variety of representations of entities and their relations, including directed and undirected graphs, multi-modal graphs (graphs which contain more than one type of vertex or edge), graphs with parallel edges (also known as multigraphs), and hypergraphs (which contain hyperedges, each of which may connect any number of vertices).
-	Mechanisms for annotating graphs, entities, and relations with metadata. These capabilities facilitate the creation of analytic tools for complex data sets that can examine the relations between entities, as well as the metadata attached to each entity and relation.
-	Implementations of a number of algorithms from graph theory, exploratory data analysis, social network analysis, and machine learning. These include routines for clustering, decomposition, optimization, random graph generation, statistical analysis, and calculation of network distances, flows, and ranking measures (centrality, PageRank, HITS, etc.)
-	A visualization framework that makes it easy to construct tools for the interactive exploration of network data. Users can choose among the provided layout and rendering algorithms, or use the framework to create their own custom algorithms.
-	Filtering mechanisms which extract subsets of a network; this allows users to focus their attention, or their algorithms, on specific portions of a network.
These capabilities make JUNG a good platform for exploratory data analysis on relational data sets.
2.6	Other
JDK 1.7.0_23
Eclipse IDE for Java EE Developers 
Eclipse Visual Editor
JFC (Swing/AWT)


[1] Yu, M., Yi, Y., Rexford, J., and Chiang, M., "Rethinking virtual network embedding: substrate support for path splitting and migration." SIGCOMM Comput. Commun. Vol.38 (2),pp. 17-29, 2008.
[2] Houidi, I., Louati, W., Zeghlache, D., “Virtual Resource Description and Clustering for Virtual Network Discovery”, ICC Workshopes 2009, IEEE, pp. 1-6, 2009
[3] Lischka, J., Holger, K., “A Virtual Mapping Algorithm based on Subgraph Isomorphism Detection”, VISA 2009, ACM 2009
[4] Zhu, Y. , Ammar, M. H., " Algorithms for Assigning Substrate Network Resources to Virtual Network Components", In Proceedings of Infocom 2006, pp. 1-12, 2006.
[5] A. Leivadeas, C. Papagianni, S. Papavassiliou, “Efficient Resource Mapping Framework over Networked Clouds via Iterated Local Search-Based Request Partitioning”, IEEE Trans. on Parallel and Distributed Systems, vol.24, no. 6, pp. 1077-1086, June 2013
[6] I. Houidi, W. Louati, W. B. Ameur, D. Zeghlache, ”Virtual network provisioning across multiple substrate network” ELSEVIER Computer Networks, vol. 55, no. 2, pp. 1011-1023, 2011
[7] M. Yu, Y. Yi, J. Rexford, and M. Chiang, “Rethinking virtual network embedding: substrate support for path splitting and migration,” ACM SIGCOMM Computer Communication Review, Vol.38, no. 2, pp. 17–29, Apr. 2008
[8] C. Papagianni, A. Leivadeas, S. Papavassiliou, V. Maglaris, C. C. Pastor, A. Monje, “On the Optimal Allocation of Virtual Resources in Cloud Computing Networks”, IEEE Trans. on Computers, vol. 62, no. 6, pp. 1060-1071, June 2013
[9] IBM ILOG CPLEX Optimizer, available at http://www-01.ibm.com/software/integration/optimization/cplex-optimizer/.
[10] GNU Linear Programming Kit, available at https://www.gnu.org/software/glpk/
[11] JUNG 2.0.1 website, available athttp://jung.sourceforge.net