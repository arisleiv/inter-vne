package gui.simulation;

import java.awt.Color;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import javax.swing.JPanel;
import javax.swing.SwingWorker;

import output.Results;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import model.Algorithm;
import model.Request;
import model.Simulation;
import model.Substrate;
import model.components.Link;
import model.components.Node;
import model.components.Server;
import model.components.SubstrateRouter;
import model.components.SubstrateSwitch;

@SuppressWarnings("rawtypes")
public class SimulationWork extends SwingWorker {

	private Simulation simulation;
	private List<Request> requests;
	private Algorithm algorithm;
	private Substrate substrate;
	SimulationFrame simulationFrame;
	JPanel simulatorContentPane;
	
	public SimulationWork(Simulation simulation, JPanel simulatorContentPane) {
		this.simulation = simulation;
		this.simulatorContentPane = simulatorContentPane;
		this.requests = simulation.getRequests();
		this.substrate = simulation.getSubstrate();
		this.algorithm = simulation.getAlgorithm();
		// Setting substrate to the algorithm
		this.algorithm.addSubstrate(this.substrate);
	}

	@Override
	protected List<Substrate> doInBackground() throws Exception {
		
		List<Substrate> resultSubstrates = new ArrayList<Substrate>();
		Substrate initSubstrate = (Substrate) substrate.getCopy();
		initSubstrate.setId(substrate.getId()+"(0)");
		resultSubstrates.add((Substrate) substrate.getCopy());

		try
	    {
		  Calendar calendar = Calendar.getInstance();
		  java.util.Date now = calendar.getTime();
		  java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(now.getTime());
		  String name =  currentTimestamp.toString();
		  String[] out = name.split(" ");
		  String[] out1 = out[1].split(":");
		  System.out.println(out[0]);
	      String filename = "input"+out[0]+"_"+out1[0]+"-"+out1[1]+"_"+ simulation.getAlgorithm().getId()+".xls";
	      WorkbookSettings ws = new WorkbookSettings();
	      ws.setLocale(new Locale("en", "EN"));
	      WritableWorkbook workbook =  Workbook.createWorkbook(new File(filename), ws);
	      WritableSheet s = workbook.createSheet("Sheet1", 0);

				    /* Format the Font */
			    WritableFont wf = new WritableFont(WritableFont.ARIAL, 
			      10, WritableFont.BOLD);
			    WritableCellFormat cf = new WritableCellFormat(wf);
			    cf.setWrap(true);
			      Label l = new Label(0,0,"Time",cf);
			      s.addCell(l);
			      l= new Label(1,0,"Acceptance",cf);
			      s.addCell(l);
			      l=new Label(2,0,"Revenue",cf);
			      s.addCell(l);
			      l=new Label(3,0,"Cost",cf);
			      s.addCell(l);
			      l=new Label (4,0,"NServer_Util_Cpu",cf);
			      s.addCell(l);
			      l=new Label (5,0,"NServer_Util_Memory",cf);
			      s.addCell(l);
			      l=new Label (6,0,"NServer_Util_DiskSpace",cf);
			      s.addCell(l);
			      l=new Label (7,0,"NRouter_Util_Cpu",cf);
			      s.addCell(l);
			      l=new Label (8,0,"NRouter_Util_Memory",cf);
			      s.addCell(l);
			      l=new Label (9,0,"NSwitch_Util_Cpu",cf);
			      s.addCell(l);
			      l=new Label (10,0,"NSwitch_Util_Memory",cf);
			      s.addCell(l);
			      l=new Label (11,0,"L_Util",cf);
			      s.addCell(l);
			    
	
		int denials = 0;
		double cost=0;
	    double revenue=0;
	    double avgHops =0;
	    int current_request=0;
		
		
		
		setProgress(0);

		// Give total time of the simulation
		int simulationTime = simulation.getEndDate();
		// Sizing progress bar
		simulationFrame.getProgressBar().setMinimum(0);
		simulationFrame.getProgressBar().setValue(0);
		simulationFrame.getProgressBar().setMaximum(simulationTime);
		
		List<Request> endingRequests;
		List<Request> startingRequests;
		
		//Find the initial Node and Link capacity of the substrate to use it in
	    //the node utilization and link utilization result.
	    //Find also the number of Substrate Servers, Switch, Routers
	    int servers=0;
	    int routers=0;
	    int switches=0;
	    double[] n_initial_Cpu=new double[substrate.getGraph().getVertexCount()];
	    double[] n_initial_Memory=new double[substrate.getGraph().getVertexCount()];
	    double[] n_initial_DiskSpace=new double[substrate.getGraph().getVertexCount()];
	    for (Node x: substrate.getGraph().getVertices()){
	    	if (x instanceof Server){
	    		n_initial_Cpu[x.getId()]=x.getCpu();
	    		n_initial_Memory[x.getId()]=x.getMemory();
	    		n_initial_DiskSpace[x.getId()]=((Server) x).getDiskSpace();
	    		servers++;
	    	}
	    	else if (x instanceof SubstrateRouter){
	    		n_initial_Cpu[x.getId()]=x.getCpu();
	    		n_initial_Memory[x.getId()]=x.getMemory();
	    		routers++;
	    	}
	    	else if (x instanceof SubstrateSwitch){
	    		n_initial_Cpu[x.getId()]=x.getCpu();
	    		n_initial_Memory[x.getId()]=x.getMemory();
	    		switches++;
	    	}
	    }
	    
	    double[] l_initial=new double[substrate.getGraph().getEdgeCount()];
	    
	    for (Link current: substrate.getGraph().getEdges()){
	    	l_initial[current.getId()]=current.getBandwidth();
	    }
/////////////////////////////////////////////////////////////////////
	    
	    Results results=new Results(substrate);
	    int counter2=0;
		

		/** for each unit of time we first release ended requests
		 * and then we send to the algorithm starting requests
		 * to perform the allocation of their resources **/
		for (int i=0; i<=simulationTime; i++) {
		
			//substrate.print();
			// Release ended simulations
			endingRequests = simulation.getEndingRequests(i);
			for (Request req : endingRequests)
				simulationFrame.addProgressText(Color.BLACK, "Releasing request "+req.getId()+"...\n");
			simulation.releaseRequests(endingRequests);
			// Allocate arriving simulations
			startingRequests = simulation.getStartingRequests(i);
			for (Request req : startingRequests)
				simulationFrame.addProgressText(Color.BLACK, "Allocating request "+req.getId()+"...\n");
			algorithm.addRequests(startingRequests);
			
			
			double[][] retres=algorithm.runAlgorithm();
  			
  			for (int ind=0; ind<startingRequests.size();ind++){
  			denials += (int)retres[ind][0];
  			cost+=retres[ind][1];
  			avgHops +=retres[ind][2];
  			
  			}
  			
  			for (Request req:startingRequests){
      			revenue=revenue+results.Generate_Revenue(req);
      			current_request++;
  			}

  		//Take results every 1000 time units
            if ((i%100)==0){
            	counter2++;
            	double[] nServer1_util=results.Node_utilization_Server_Cpu(n_initial_Cpu);
            	double[] nServer2_util=results.Node_utilization_Server_Memory(n_initial_Memory);
            	double[] nServer3_util=results.Node_utilization_Server_DiskSpace(n_initial_DiskSpace);
            	double[] nRouter1_util=results.Node_utilization_Router();
            	double[] l_util=results.Link_utilization(l_initial);
            	double nServer1_average=0;
            	double nServer2_average=0;
            	double nServer3_average=0;
            	double nRouter1_average=0;
            	double nRouter2_average=0;
            	double nSwitch1_average=0;
            	double nSwitch2_average=0;
            	double l_average=0;
            	for (int j=0;j<substrate.getGraph().getVertexCount();j++){
            		nServer1_average += nServer1_util[j];
            		nServer2_average += nServer2_util[j];
            		nServer3_average += nServer3_util[j];
            		nRouter1_average += nRouter1_util[j];
            	}
            	for (int j=0;j<substrate.getGraph().getEdgeCount();j++)
            		l_average=l_average+l_util[j];
            	
            	Number n = new Number(0,counter2,i);
                s.addCell(n);
                Number n1 = new Number(1,counter2,results.Acceptance_Ratio(denials, current_request));
                s.addCell(n1);
                Number n2 = new Number(2,counter2,revenue/i);
                s.addCell(n2);
                Number n3 = new Number(3,counter2,cost/(current_request-denials));
                s.addCell(n3);
                Number n4 = new Number(4,counter2,(nServer1_average/servers));
                s.addCell(n4);
                Number n5 = new Number(5,counter2,(nServer2_average/servers));
                s.addCell(n5);
                Number n6 = new Number(6,counter2,(nServer3_average/servers));
                s.addCell(n6);
                Number n7 = new Number(7,counter2,(nRouter1_average/routers));
                s.addCell(n7);
                Number n8 = new Number(8,counter2,(nRouter2_average/routers));
                s.addCell(n8);
                Number n9 = new Number(9,counter2,(nSwitch1_average/switches));
                s.addCell(n9);
                Number n10 = new Number(10,counter2,(nSwitch2_average/switches));
                s.addCell(n10);
                Number n11 = new Number(11,counter2,(l_average/substrate.getGraph().getEdgeCount()));
            	s.addCell(n11);
            	Number n12 = new Number(12,counter2,avgHops/(current_request-denials));
            	
                s.addCell(n12);
            }
      
			
			// Progress++
			setProgress(100*i/simulationTime);
			System.out.println("Progress: "+getProgress());
			simulationFrame.addProgressText(Color.BLACK, "Simulation time "+i+" done\n");
			simulationFrame.getProgressBar().setValue(i+1);
			System.out.println("in tge init b ////////////////////////////////////////////////////////");
			//substrate.print();
			Substrate iterationSubstrate = (Substrate) substrate.getCopy();
			iterationSubstrate.setId(substrate.getId()+"("+(i+1)+")");
			resultSubstrates.add(iterationSubstrate);

		}
		
		System.out.println();
		System.out.println("simulation time: "+simulationTime);
		System.out.println("denials: "+denials);
		System.out.println("current request: "+requests.size());

        
		workbook.write();
	    workbook.close(); 
	    }
	    catch (IOException e)
	    {
	      e.printStackTrace();
	    }
	    catch (WriteException e)
	    {
	      e.printStackTrace();
	    }
		
        return resultSubstrates;
	}
	
	 /*
     * Executed in event dispatching thread
     */
    @SuppressWarnings("unchecked")
	@Override
    public void done() {
    	Toolkit.getDefaultToolkit().beep();
    	if (!isCancelled()) {
    		simulationFrame.addProgressText(Color.BLACK, "Simulation finished\n");
    		simulationFrame.getCancelButton().setEnabled(false);
    		// Show panel with the state of the substrate at each step
			List<Substrate> resultSubstrates = new ArrayList<Substrate>();
			try {
				resultSubstrates = (List<Substrate>) this.get();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (resultSubstrates.size()==0) {
				//TODO Error!
				return;
			}
			// Setting visible the ResultsFrame
			ResultsFrame rf = new ResultsFrame(simulatorContentPane,
					"Substrate status during the simulation",
					resultSubstrates);
			rf.pack();
			rf.setVisible(true);
    	}
    	else simulationFrame.addProgressText(Color.RED, "Simulation canceled\n");
    	simulationFrame.getProgressDescription().setEditable(false);
//    	simulationDialog.setVisible(false);
    }

	public void setDialog(SimulationFrame simulationDialog) {
		this.simulationFrame = simulationDialog;
	}

	public Substrate getSubstrate() {
		return substrate;
	}

}
