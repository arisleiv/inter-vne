package model;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import edu.uci.ics.jung.graph.util.Pair;

import model.components.Link;
import model.components.Node;
import model.components.RequestRouter;
import model.components.RequestSwitch;
import model.components.Server;
import model.components.SubstrateRouter;
import model.components.SubstrateSwitch;
import model.components.VirtualMachine;

public class ResourceMapping {
	@SuppressWarnings("unused")
	private Request request;
	private LinkedHashMap<Node, Node> nodeMap=null; //request-real 
	private double[][][] flows;
	@SuppressWarnings("unused")
	private int aug =0;
	private boolean denied=false;
	
	public ResourceMapping(Request req){
		this.request=req;
	}

	public void setNodeMapping(LinkedHashMap<Node, Node> map){
		this.nodeMap=map;
	}
	
	public void setLinkMapping(double[][][] f){
		this.flows=f;
	}
	
	public void denied() { this.denied=true; }
	public void accepted() { this.denied=false; }
	
	public boolean isDenied() { return this.denied;}
	public void setAugmented() { this.aug=1;}
	
	@SuppressWarnings("rawtypes")
	public void reserveNodes(Substrate sub){
		Collection v = sub.getGraph().getVertices();
		Iterator itr_sub = v.iterator();
		while(itr_sub.hasNext()){
			Node subNode =  (Node) itr_sub.next();
			int aug_ID=  subNode.getId();

			Collection c = this.nodeMap.entrySet();
			Iterator itr = c.iterator();

			while(itr.hasNext()){
				 Map.Entry entry = (Map.Entry)itr.next();
				 if(aug_ID == ((Node)entry.getValue()).getId()) {
				 	if (((Node)entry.getKey()) instanceof VirtualMachine)  {
				    	 ((Server)subNode).setCpu( ((Server)entry.getValue()).getCpu()-((VirtualMachine)entry.getKey()).getCpu());
				    	 ((Server)subNode).setMemory( ((Server)entry.getValue()).getMemory()-((VirtualMachine)entry.getKey()).getMemory());
				    	 ((Server)subNode).setDiskSpace( ((Server)entry.getValue()).getDiskSpace()-((VirtualMachine)entry.getKey()).getDiskSpace());
				    	 ((Server)subNode).setStress( ((Server)entry.getValue()).getStress()+1);
			   	 	}
				    else if ((((Node)entry.getKey()) instanceof RequestRouter)){
				    	subNode.setStress(subNode.getStress()+1);
				    	((SubstrateRouter)subNode).setLogicalInstances(((SubstrateRouter)subNode).getLogicalInstances()-1);
				    	 	}
				    else if ((((Node)entry.getKey()) instanceof RequestSwitch)) {
				    	((SubstrateSwitch)subNode).setLogicalInstances(((SubstrateSwitch)subNode).getLogicalInstances()-1);
				    	subNode.setStress(subNode.getStress()+1);
		    	 	}
				 }
			}		
		}		

	}
	
			
	@SuppressWarnings("rawtypes")
	public void releaseNodes(Substrate sub){
		Collection v = sub.getGraph().getVertices();
		Iterator itr_sub = v.iterator();
		while(itr_sub.hasNext()){
			Node subNode =  (Node) itr_sub.next();
			int aug_ID=  subNode.getId();
			//if (this.aug==1)  aug_ID += this.request.getGraph().getVertexCount();
			if(this.nodeMap!=null) {
			
			Collection c = this.nodeMap.entrySet();
			Iterator itr = c.iterator();

			while(itr.hasNext()){
				 Map.Entry entry = (Map.Entry)itr.next();
				 if(aug_ID == ((Node)entry.getValue()).getId()) {
				 	if (((Node)entry.getKey()) instanceof VirtualMachine  )  {
				    	 ((Server)subNode).setCpu( ((Server)subNode).getCpu()+((VirtualMachine)entry.getKey()).getCpu());
				    	 ((Server)subNode).setMemory( ((Server)subNode).getMemory()+((VirtualMachine)entry.getKey()).getMemory());
				    	 ((Server)subNode).setDiskSpace( ((Server)subNode).getDiskSpace()+((VirtualMachine)entry.getKey()).getDiskSpace());
				    	 subNode.setStress(subNode.getStress()-1);
			   	 	}
				    else if ((((Node)entry.getKey()) instanceof RequestRouter) ){
				    	subNode.setStress(subNode.getStress()-1);
				    	((SubstrateRouter)subNode).setLogicalInstances(((SubstrateRouter)subNode).getLogicalInstances()+1);
				    }
				    else if ((((Node)entry.getKey()) instanceof RequestSwitch) ) {
				    	((SubstrateSwitch)subNode).setLogicalInstances(((SubstrateSwitch)subNode).getLogicalInstances()+1);
				    	subNode.setStress(subNode.getStress()-1);
		    	 	}
				 }
			}
			
			
		
			}
		}
		
	}
	
	public void reserveLinks(Substrate sub){
		
		for (int k=0;k<this.flows.length;k++){
		
			for (int i=0;i<sub.getGraph().getVertexCount();i++){
				for (int j=0;j<sub.getGraph().getVertexCount();j++){
					if (this.flows[k][i][j]!=0){
						Collection<Link> edges = sub.getGraph().getEdges();
						for (Link current : edges){
							Pair<Node> currentNodes =sub.getGraph().getEndpoints(current);
							int node1 = currentNodes.getFirst().getId();
							int node2= currentNodes.getSecond().getId();
							if ( ( (node1==i)&&(node2==j)) || ( (node1==j)&&(node2==i)) ){
								current.setBandwidth(current.getBandwidth()-((int)this.flows[k][i][j]));
								current.setStress(current.getStress()+1);
							}							
						}
					}
				}
			}
		}
		
	}
	
	
	
	public void releaseLinks(Substrate sub){
		if(this.nodeMap!=null) {
			for (int k=0;k<this.flows.length;k++){
				for (int i=0;i<sub.getGraph().getVertexCount();i++){
					for (int j=0;j<sub.getGraph().getVertexCount();j++){
						if (this.flows[k][i][j]!=0){
							Collection<Link> edges = sub.getGraph().getEdges();
							for (Link current : edges){
								Pair<Node> currentNodes =sub.getGraph().getEndpoints(current);
								int node1 = currentNodes.getFirst().getId();
								int node2= currentNodes.getSecond().getId();
								if ( ( (node1==i)&&(node2==j)) || ( (node1==j)&&(node2==i)) ){
									current.setBandwidth(current.getBandwidth()+((int)this.flows[k][i][j]));
									current.setStress(current.getStress()-1);
								}
							}
	
						}
					}
				}
			}
		}
	}
	
	@SuppressWarnings("rawtypes")
	public double computeCost(Substrate sub){
		
		double cost=0;
		for (int k=0;k<this.flows.length;k++){
			for (int i=0;i<sub.getGraph().getVertexCount();i++){
				for (int j=0;j<sub.getGraph().getVertexCount();j++){
					if (this.flows[k][i][j]!=0){
						cost+=this.flows[k][i][j];
					}
				}
			}
		}
		
	
		if (cost!=0){
		Collection v = sub.getGraph().getVertices();
		Iterator itr_sub = v.iterator();
		while(itr_sub.hasNext()){
			Node subNode =  (Node) itr_sub.next();
			int aug_ID=  subNode.getId();

			Collection c = this.nodeMap.entrySet();
			Iterator itr = c.iterator();

			while(itr.hasNext()){
				 Map.Entry entry = (Map.Entry)itr.next();
				 if(aug_ID == ((Node)entry.getValue()).getId()) {
				 	if (((Node)entry.getKey()) instanceof VirtualMachine)  {
				 		cost+=((VirtualMachine) entry.getKey()).getCpu()+((VirtualMachine)entry.getKey()).getMemory()+((VirtualMachine)entry.getKey()).getDiskSpace();
			   	 	}
				    else if ((((Node)entry.getKey()) instanceof RequestRouter)){
				    	cost += 15;
		    	 	}
				    else if ((((Node)entry.getKey()) instanceof RequestSwitch)) {
				    	cost += 15;
				    }
				 }
			}
		
		}
		}
		
		return cost;

	}
	
	@SuppressWarnings("rawtypes")
	void printNodeMapping(){
		Collection c = this.nodeMap.entrySet();
		Iterator itr = c.iterator();
		while(itr.hasNext()){
			 Map.Entry entry = (Map.Entry)itr.next();
			 
			 System.out.println("Virtual " +((Node)entry.getKey()).getName()+  " cpu  " +((Node)entry.getKey()).getCpu() + 
					 " Real " + ((Node) entry.getValue()).getName() + " cpu "  + ((Node) entry.getValue()).getCpu());
		}
	}

	void printLinkMapping(Substrate sub){
		
		for (int k=0;k<this.flows.length;k++){
			System.out.println("For Virtual link " + k );
			for (int i=0;i<sub.getGraph().getVertexCount();i++){
				for (int j=0;j<sub.getGraph().getVertexCount();j++){
					if (this.flows[k][i][j]!=0){
						Collection<Link> edges = sub.getGraph().getEdges();
						for (Link current : edges){
							Pair<Node> currentNodes =sub.getGraph().getEndpoints(current);
							int node1 = currentNodes.getFirst().getId();
							int node2= currentNodes.getSecond().getId();
								if ( ( (node1==i)&&(node2==j)) || ( (node1==j)&&(node2==i)) )
									 System.out.println("START "+ currentNodes.getFirst().getName()+ " end " +currentNodes.getSecond().getName()+ " flow " +  
											 this.flows[k][i][j] +  " mapped tp Real " + 
											 current.getId() + " with bw " +current.getBandwidth());						
						}
					}
				}			
			}
		
		}
	}
}


