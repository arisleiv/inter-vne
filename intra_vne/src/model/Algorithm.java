package model;

import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import output.Results;

import tools.NodeComparator;

import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.util.EdgeType;
import edu.uci.ics.jung.graph.util.Pair;

import model.components.Link;
import model.components.Node;
import model.components.RequestRouter;
import model.components.RequestSwitch;
import model.components.Server;
import model.components.SubstrateLink;
import model.components.SubstrateRouter;
import model.components.SubstrateSwitch;
import model.components.VirtualMachine;

/**
 * DUMMY Algorithm Class.
 */

public class Algorithm {
	public static final int rVine=0; 

	private String id;
	private String state;
	private String provision;
	private Substrate substrate ;
	private List<Request> reqs;
    private  static int  MAX_TYPES = 3;

	public Algorithm(String id) {
		this.id = id;
		this.state = "available";
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	

	public void addSubstrate(Substrate substrate) {
		this.substrate = substrate;
	}
	
	public void addRequests(List<Request> reqs) {
		this.reqs = reqs;
	}

	public double[][] runAlgorithm (){
		double[][] retres = null;
		
		if (this.id.contentEquals("NCM")) 
				retres=NCM();
		else if (this.id.contentEquals("GSP"))
			    retres=GSP();
		else if (this.id.contentEquals("GMCF"))
			     retres=GMCF();
		
		return retres;
	}
	
public static void waiting (int n){
        
        long t0, t1;

        t0 =  System.currentTimeMillis();

        do{
            t1 = System.currentTimeMillis();
        }
        
        while (t1 - t0 < n);
    }

		
	private double[][] GMCF() {
		int node_den=0;
		int link_den=0;
		int link_den2=0;
		double[][] retres=new double[reqs.size()][10];
		int req_counter = 0;
		for (Request req: reqs){ //for every request
			

			int denial=0;
			boolean flag=true;
			double[][][] flowReserved= new double[req.getGraph().getEdgeCount()][substrate.getGraph().getVertexCount()][substrate.getGraph().getVertexCount()];
						
			LinkedHashMap<Node, Node> phi = new LinkedHashMap<Node, Node>(); // request-real
			//array holding virtual to substrate link mapping.
		
			
			//duplicate substrate graph   
			Substrate sub2 = (Substrate) this.substrate.getCopy();

			   
			//get bandwidth on substrate links.
			double[][] capTable = getCapTable(sub2.getGraph()) ;
			//get bandwidth of request links
			double[][] demands = getBandTable(req.getGraph());
			//number of substrate nodes
			int initSubNodeNum=this.substrate.getGraph().getVertexCount();
			//number of virtual nodes
			int reqNodeNum=req.getGraph().getVertexCount();
			//number of virtual links
			int reqLinksNum=req.getGraph().getEdgeCount();
			   

			phi =  GreedyNodeMapping.GND(req, sub2, capTable, provision);
			//  print_map(phi, "printing node mapping (requested - real)"); 
				
		if ((phi!=null)&&(phi.size()==req.getGraph().getVertexCount())){
			
				/////////////////////////////////////////////////////////////////
				//Step 2 implement MCF//////////////////////////////////////////
				///////////////////////////////////////////////////////////////

			try{
				
			IloCplex cplex1 = new IloCplex();
			//cplex1.setParam(IloCplex.DoubleParam.TiLim, 15);
			
			IloNumVar[][][] f_mcf = new IloNumVar[reqLinksNum][][];
			for (int k=0;k<reqLinksNum;k++){
				f_mcf[k]=new IloNumVar[initSubNodeNum][];
				for (int i=0;i<initSubNodeNum;i++){
					f_mcf[k][i]=cplex1.numVarArray(initSubNodeNum, 0, Double.MAX_VALUE);
				}
			}
			
			IloLinearNumExpr flows_mcf = cplex1.linearNumExpr();
			
			for (int i=0;i<initSubNodeNum; i++){ 
				for (int j=0;j<initSubNodeNum; j++){
					for (int k =0; k<reqLinksNum; k++) {
					flows_mcf.addTerm(1.0, f_mcf[k][i][j]);
					}
				}
			}

			cplex1.addMinimize(flows_mcf);
			
			
			/////////////////////////////////////////////
			//find the origins of the virtual link//////
			///////////////////////////////////////////
			int[] o=new int[reqLinksNum];
			int[] d=new int[reqLinksNum];
			int ct=0;
			for (int i=0;i<reqNodeNum;i++){
				for (int j=0;j<reqNodeNum;j++){
					if ((j>i)&&(demands[i][j]!=0)){
						o[ct]=i;
						d[ct]=j;
						ct++;
					}
				}
			}
			
			
			int[] o2=new int[reqLinksNum];
			int[] d2=new int[reqLinksNum];
			
			//Find the source and destination of the substrate paths to be
			for (Link link:req.getGraph().getEdges()){
				Pair<Node> x =  req.getGraph().getEndpoints(link);
				//the source virtual node
				int virt_source=x.getFirst().getId();
				//the destination virtual node
				int virt_dest = x.getSecond().getId();
				
				//find which substrates nodes correspond to virtual source and destination
			    @SuppressWarnings("rawtypes")
				Set entries = phi.entrySet();
			    @SuppressWarnings("rawtypes")
				Iterator iterator = entries.iterator();
			    while (iterator.hasNext()) {
			         @SuppressWarnings("rawtypes")
					Map.Entry entry = (Map.Entry)iterator.next();
			         if (virt_source==((Node)entry.getKey()).getId()){
			        	 o2[link.getId()]=((Node)entry.getValue()).getId();
			         }
			         if (virt_dest==((Node)entry.getKey()).getId()){
			        	 d2[link.getId()]=((Node)entry.getValue()).getId();
			         }
			    }
			}
						
			double[][] tmpReqLinks = getCapTable(req.getGraph());

			//flow reservation1		
			for (int k=0;k<reqLinksNum;k++){
				for (int i=0;i<initSubNodeNum;i++){
					IloLinearNumExpr capReq = cplex1.linearNumExpr();
					if ((o2[k])!=i&&(d2[k])!=i){
						for (int j=0;j<initSubNodeNum;j++){
							capReq.addTerm(1,f_mcf[k][i][j]);
							capReq.addTerm(-1,f_mcf[k][j][i]);
						}
					}
					cplex1.addEq(capReq,0);
				}
			}
		
		//flow reservation2
		for(int k=0;k<reqLinksNum;k++){
			IloLinearNumExpr capReq = cplex1.linearNumExpr();
			for (int i=0;i<initSubNodeNum;i++){
				capReq.addTerm(1,f_mcf[k][o2[k]][i]);
				capReq.addTerm(-1,f_mcf[k][i][o2[k]]);
			}
			double reCap = 0;
			reCap=tmpReqLinks[o[k]][d[k]];
			cplex1.addEq(capReq,reCap);
		}

		//flow reservation3
		for(int k=0;k<reqLinksNum;k++){
			IloLinearNumExpr capReq = cplex1.linearNumExpr();
			for (int i=0;i<initSubNodeNum;i++){
				capReq.addTerm(1,f_mcf[k][d2[k]][i]);
				capReq.addTerm(-1,f_mcf[k][i][d2[k]]);
			}
			double reCap = 0;
			reCap=-tmpReqLinks[o[k]][d[k]];
			cplex1.addEq(capReq,reCap);
		}					
			
			//Link constraint
			for (int i=0;i<initSubNodeNum; i++){
				for (int j=0;j<initSubNodeNum; j++){
					IloNumExpr expr1 =cplex1.prod(f_mcf[0][0][0],0);
					IloNumExpr flowSum =cplex1.prod(f_mcf[0][0][0],0);				
					for (int k=0;k<reqLinksNum; k++){
						flowSum = cplex1.sum(cplex1.prod(f_mcf[k][i][j],1), 
								cplex1.prod(f_mcf[k][j][i],1));
						expr1 =cplex1.sum(expr1,flowSum);
					}
					double capacity =capTable[i][j];
					cplex1.addLe(expr1,capacity);	
				}
			}
						
			
			if ( cplex1.solve() ) {
			//cplex1.output().println("Solution status = " + cplex1.getStatus()); 
			//The returned value tells you what ILOG cplex1 found out about the model: whether it found the optimal solution or only a feasible solution, whether it proved the model to be unbounded or infeasible, or whether
			//nothing at all has been determined at this point.
			
			//cplex1.output().println("Solution value = " + cplex1.getObjValue());
			//int ncols1 = cplex1.getNcols();
			//cplex1.output().println("ncols1: " + ncols1);

		
			for (int k=0;k<reqLinksNum;k++){
				for (int i=0;i<initSubNodeNum;i++){
					for (int j=0;j<initSubNodeNum;j++){
						flowReserved[k][i][j]=cplex1.getValue(f_mcf[k][i][j]);
					}
				}
			}
		
				
			}///cplex1
			else{
				flag=false;
				denial++;
				link_den++;
			}
				
			cplex1.end();
			
				} catch (IloException e) {
				System.err.println("Concert exception caught: " + e);
				}
			

				
		}//end of flag due to node mapping
		else{
			denial++;
			node_den++;
		}


	
		//////////////////////////////////////////////////////////
		//Update the capacities//////////////////////////////////
		////////////////////////////////////////////////////////
		
			
		ResourceMapping resMap=req.getRMap();

		double cost=0;
		if ((flag)&&(phi!=null)){
			resMap.setNodeMapping(phi);
			resMap.setLinkMapping(flowReserved);
			resMap.reserveNodes(this.substrate);
			resMap.reserveLinks(this.substrate);
			cost= resMap.computeCost(this.substrate);
			resMap.accepted();
		}
		else{
			resMap.denied();
		}
		req.setRMap(resMap);

		Results result=new Results(this.substrate);
		double avgHops=result.avgHops(flowReserved);
		
		retres[req_counter][0]=denial;
		retres[req_counter][1]=cost;
		retres[req_counter][2]=avgHops;
		retres[req_counter][3]=link_den2;
		retres[req_counter][4]=node_den;
		retres[req_counter][5]=link_den;

		if (denial==0){
			if (req.getProv()=="soft") retres[req_counter][7]=1; //soft provisioningn counter
			else retres[req_counter][8]=1; //hard provisioning counter
		}
		req_counter++;
		}

		return retres;
	}


/////////////////////////////////GSP/////////////////////////////////////////////////////////////////////////
	public double[][] GSP(){
	int node_den=0;
	int link_den=0;
	@SuppressWarnings("unused")
	boolean denied=false;
	int req_counter=0;
	double[][] retres=new double[reqs.size()][9];
	
	
	//////////////////REQUESTS//////////////////////////
	
	
	for (Request req: reqs){ //for every request
		denied=false;
		int denial=0;
			
		   LinkedHashMap<Node, Node> phi = new LinkedHashMap<Node, Node>(); // request-real
		   //array holding virtual to substrate link mapping.
		   double[][][] flowReserved= new double[req.getGraph().getEdgeCount()][substrate.getGraph().getVertexCount()]
		                                                                        [substrate.getGraph().getVertexCount()];
		   //duplicate substrate graph   
		  
		   Substrate sub2 = (Substrate) this.substrate.getCopy();
		   			
		   
		   //get bandwidth on substrate links.
		   double[][] capTable = getCapTable(sub2.getGraph()) ;
		   
		   		   
		   // req.print();
		   phi =  GreedyNodeMapping.GND(req, sub2, capTable, provision);

		   
		   /////////////////////////////////////////////////////////////////
		   //Step 2 implement Dijkstra/////////////////////////////////////
		   ///////////////////////////////////////////////////////////////
			if (phi!=null){
				flowReserved =  Dijkstra.GLM(req, this.substrate, capTable, phi);
				//print_map_link(initSubNodeNum, req, flowReserved, "printing link mapping (requested - real)");
				if (flowReserved==null){
					link_den++;
					//denial++;
				}
				//Check node Mapping
				if (phi.size()!=req.getGraph().getVertexCount()){
					System.out.println("ITS NODE MAPPING FAULT");
					System.exit(0);
				}
			}
			else{
				node_den++;
			}
			
				
			ResourceMapping resMap=req.getRMap();

			if ((phi!=null)&&(flowReserved!=null)){
				resMap.setNodeMapping(phi);
				req.getRMap().setLinkMapping(flowReserved);
				req.getRMap().reserveNodes(this.substrate);
				req.getRMap().reserveLinks(this.substrate);
				req.getRMap().accepted();
			}
			else{
				req.getRMap().denied();
				denial++;
				System.out.println("denial end");
			}
			req.setRMap(resMap);
					

			
			///Take results
			Results results=new Results(this.substrate);
			if ((phi!=null)&&(flowReserved!=null)) {
				double cost=results.Cost_Embedding(flowReserved, req);
				double avgHops=results.avgHops(flowReserved);
				retres[req_counter][1]=cost;
				retres[req_counter][2]=avgHops;
				System.out.println("Coost="+ cost );
				System.out.println("hooops= "+avgHops);
			}
			
			
			
			
			retres[req_counter][0]=denial;
			retres[req_counter][4]=node_den;
			retres[req_counter][5]=link_den;

			if (denial==0){
				if (req.getProv()=="soft") retres[req_counter][7]=1; //soft provisioningn counter
				else retres[req_counter][8]=1; //hard provisioning counter
			}
			req_counter++;
			
		}
		return retres;
	}
	
		
	
//-----------------------------------------NCM*************************
	
	/**
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private double[][] NCM() {
		double[][] retres=new double[reqs.size()][9];
		boolean denied =false;
		int denials1=0;
		@SuppressWarnings("unused")
		int denials2=0;
		int algo_denials=0;
		int req_counter=0;
	///////////////////////////////////////////////////////////////////////////

		for (Request req: reqs){ //for every request
			int denial=0;
			denied=false;
			double[][][] fi=new double[req.getGraph().getEdgeCount()][this.substrate.getGraph().getVertexCount()][this.substrate.getGraph().getVertexCount()];
			LinkedHashMap<Node, Node> NodeMapping =new LinkedHashMap<Node,Node>();
					
			/// create augmented graph 
			// add substrate
			
			Substrate augmentedSubstrate = new Substrate("augmentedSubstrate");
			Substrate substrateCopy = (Substrate) this.substrate.getCopy();
			
			//change ids in substrate copy
			for (Node n: substrateCopy.getGraph().getVertices() ){
				int current_id = n.getId();
				n.setId(current_id+req.getGraph().getVertexCount());
			}

			////////////////////////////////////////////////////////////////////
			//////////// add substrate to augmented
			for (Link current : substrateCopy.getGraph().getEdges()){
				Pair<Node> x =  substrateCopy.getGraph().getEndpoints(current);
				augmentedSubstrate.getGraph().addEdge(current, x.getFirst(), x.getSecond(),  EdgeType.UNDIRECTED);
				augmentedSubstrate.getGraph().addVertex(x.getFirst());
				augmentedSubstrate.getGraph().addVertex(x.getSecond());
			}

			//////////// add req to augmented///////////////////////////////

			Collection<Node> tmpReqNodes = getNodes(req.getGraph());
			int subLinkNum = augmentedSubstrate.getGraph().getEdgeCount();

			for (Node x : tmpReqNodes){
				augmentedSubstrate.getGraph().addVertex(x);
				for (Node subNode : substrateCopy.getGraph().getVertices()){
					int LinkID = subLinkNum++;

					SubstrateLink l = new SubstrateLink(LinkID,(int) 2.147483647E5);
					augmentedSubstrate.getGraph().addEdge(l, x, subNode, EdgeType.UNDIRECTED);
				}
			}
	
			double[][] augCapTable = getCapTable(augmentedSubstrate.getGraph()) ;

			// run cplex on relaxed problem

			try {

				IloCplex cplex = new IloCplex();
				//If the solution is not found within 10 minutes reject the request
				cplex.setParam(IloCplex.DoubleParam.TiLim, 600);
				cplex.setParam(IloCplex.DoubleParam.ObjULim,Double.MAX_VALUE);
				cplex.setParam(IloCplex.DoubleParam.ObjLLim,-1*Double.MAX_VALUE);
				IloCplex cplex1 = new IloCplex();
				cplex1.setParam(IloCplex.DoubleParam.TiLim, 600);
				cplex1.setParam(IloCplex.DoubleParam.ObjULim,Double.MAX_VALUE);
				cplex1.setParam(IloCplex.DoubleParam.ObjLLim,-1*Double.MAX_VALUE);

				// substate copy has changed ids
				ArrayList<Node> tmpSubNodesList = new ArrayList<Node>();
				tmpSubNodesList = 	(ArrayList<Node>) getNodes(substrateCopy.getGraph());
				Collections.sort(tmpSubNodesList,new NodeComparator());

				ArrayList<Node> tmpReqNodesList = new ArrayList<Node>();
				tmpReqNodesList = 	(ArrayList<Node>) getNodes(req.getGraph());
				Collections.sort(tmpReqNodesList,new NodeComparator());
				
			
			
				int initSubNodeNum = tmpSubNodesList.size();
				int reqNodeNum = tmpReqNodes.size();
				int reqLinksNum = req.getGraph().getEdgeCount();
				int augGraph = initSubNodeNum+reqNodeNum;
	 
				//create x,f continuous variables, all with bounds lb and ub
			
				IloNumVar[][][] x = new IloNumVar[reqLinksNum][][];
				for (int k=0;k<reqLinksNum;k++){
					x[k]=new IloNumVar[augGraph][];
					for (int i=0;i<augGraph;i++){
						x[k][i]=cplex.numVarArray(augGraph, 0, 1);//, IloNumVarType.Int);
					}
				}

				IloNumVar[][][] f = new IloNumVar[reqLinksNum][][];
				for (int k=0;k<reqLinksNum;k++){
					f[k]=new IloNumVar[augGraph][];
					for (int i=0;i<augGraph;i++){
						f[k][i]=cplex.numVarArray(augGraph, 0, 1000000);
					}
				}

				////////////////////////////////////////////////////////////////////////
				//It builds the first summation of the objective function//////////////
				//////////////////////////////////////////////////////////////////////
				IloLinearNumExpr flows = cplex.linearNumExpr();
				for (int i=reqNodeNum;i<augGraph; i++){ 
					for (int j=reqNodeNum;j<augGraph; j++){
						for (int k =0; k<reqLinksNum; k++) {
						flows.addTerm(1.0/(augCapTable[i][j]+0.000001), f[k][i][j]);
						//flows.addTerm(1.0, f[k][i][j]);
						}
					}
				}

				///////////////////////////revise////////////////////////////////////////////////////////
				/////////////////////////////////////////////////////////////////////

				///////////////////////////////////////////////////
				//Find how many routers and servers we have///////
				/////////////////////////////////////////////////
				LinkedHashMap<String, Integer> tmp_req = getType(tmpReqNodesList);
				int v_types =  tmp_req.size();
				/////////////////
				LinkedHashMap<String, Integer> tmp_sub = getType(tmpSubNodesList);
				int s_types =  tmp_sub.size();
				int[] ctypes=new int[s_types];
				ctypes[0]=1;
				
				ArrayList<ArrayList<Integer>> Vv  =createArray(tmpReqNodesList);
				ArrayList<ArrayList<Integer>> Vs = createArray(tmpSubNodesList);
			    
				System.out.println("Vv: "+Vv);
				System.out.println("Vs: "+Vs);
    			////////////////////////////////////////////////////////////////
				//Build arrays for the different functional characteristics////
				//////////////////////////////////////////////////////////////
				int[][] Vcost= getCapacities(tmpReqNodesList);
				int[][] Scost = getCapacities(tmpSubNodesList);
				


				///////////////////////////////revise/////////////////////////////////////////////////

				////////////////////////////////////////////////////////////
				//building the second summation of the objective function//
				//////////////////////////////////////////////////////////
				IloLinearNumExpr capacities = cplex.linearNumExpr();
				for (int k=0;k<reqLinksNum;k++){
					for (int i=0;i<v_types;i++){
						for (int w : Vs.get(i)){
							for (int m : Vv.get(i)){
								double total_cap=0;
								for (int j=0;j<3;j++){
									total_cap= ((total_cap+Vcost[j][m]/(Scost[j][w]+0.000001)));
								}
								capacities.addTerm(total_cap,x[k][m][w+reqNodeNum]);
							}
						}
					}
				}

				
				//Third summation
				IloLinearNumExpr hops = cplex.linearNumExpr();
				for (int k =0; k<reqLinksNum; k++) {
					for (int i=reqNodeNum;i<augGraph; i++){ 
						for (int j=reqNodeNum;j<augGraph; j++){
							hops.addTerm(1.0/(augCapTable[i][j]+0.000001), x[k][i][j]);
						}
					}
				}
				
				//The objective function
				IloNumExpr expr = cplex.sum(flows,capacities);
				expr = cplex.sum(expr,hops);
				
				//create objective minimization
				cplex.addMinimize(expr);


				
				//////////////////////////////////////////////////////////
				//Link constraint////////////////////////////////////////
				////////////////////////////////////////////////////////
				for (int k=0;k<reqLinksNum; k++){
					for (int i=0;i<augGraph; i++){
						for (int j=0;j<augGraph; j++){
							IloNumExpr flowSum =cplex.prod(x[0][0][0],0);
							IloLinearNumExpr availBW = cplex.linearNumExpr();
							flowSum = cplex.sum(cplex.prod(f[k][i][j],1), 
										cplex.prod(f[k][j][i],1));
							
							double capacity = augCapTable[i][j];
							availBW.addTerm(capacity,x[k][i][j]);
							cplex.addLe(flowSum,availBW);	
						}
					}		
				}


				//Link Constraint 2
	
				for (int i=0;i<augGraph; i++){
					for (int j=0;j<augGraph; j++){
						IloNumExpr expr1 =cplex.prod(x[0][0][0],0);
						IloNumExpr flowSum =cplex.prod(x[0][0][0],0);
						
						for (int k=0;k<reqLinksNum; k++){
							flowSum = cplex.sum(cplex.prod(f[k][i][j],1), 
									cplex.prod(f[k][j][i],1));
							expr1 =cplex.sum(expr1,flowSum);
						}
						double capacity = augCapTable[i][j];
					
						cplex.addLe(expr1, capacity);
					}
				}

			
				//node constraint for virtual machines and servers

				for (int k=0;k<reqLinksNum;k++){
					for (int i=0;i<s_types;i++){
						for (int w : Vs.get(i)){
							for (int m : Vv.get(i)){
								for (int j=0;j<3;j++){
									double cpuSub=Scost[j][w];
									cplex.addLe(cplex.prod(Vcost[j][m], x[k][m][w+reqNodeNum]),cpuSub);
								}
							}
						}
					}
				}

				//Constraint 8
				for (int k=0;k<reqLinksNum;k++){
					for (int i=0;i<s_types;i++){
						for (int w : Vs.get(i)){
							IloLinearNumExpr capReq = cplex.linearNumExpr();
							for (int m : Vv.get(i)){
								capReq.addTerm(1,x[k][m][w+reqNodeNum]);
							}
							cplex.addLe(capReq,1);
						}
					}
				}

				//meta-constraint 9 
				for (int k=0;k<reqLinksNum;k++){
					for (int m=0;m<reqNodeNum;m++){
						IloLinearNumExpr capReq = cplex.linearNumExpr();
							for (int w=reqNodeNum;w<augGraph;w++){
								capReq.addTerm(1,x[k][m][w]);
						}
							cplex.addLe(capReq, 1);
					}
				}
				
				//Constraint 10
				for (int k=0;k<reqLinksNum;k++){
					for (int i=0;i<s_types;i++){
						for (int m : Vv.get(i)){
							IloLinearNumExpr capReq = cplex.linearNumExpr();
							for (int w : Vs.get(i)){
								capReq.addTerm(1,x[k][m][w+reqNodeNum]);
							}
							cplex.addEq(capReq,1);
						}
					}
				}

				//Constraint 11
				for (int k=0;k<reqLinksNum;k++){
					for (int i=0;i<augGraph; i++){
						for (int j=0;j<augGraph; j++){
								IloLinearNumExpr capReq1 = cplex.linearNumExpr();
								IloLinearNumExpr capReq2 = cplex.linearNumExpr();
								capReq1.addTerm(1,x[k][i][j]);
								capReq2.addTerm(1,x[k][j][i]);
								cplex.addEq(capReq1,capReq2);
						}
					}
				}
				
			
				
				//Constraint 12
				
				for (int k=0;k<reqLinksNum;k++){
					for (int i=0;i<augGraph;i++){
						for (int j=0;j<augGraph;j++){
							IloNumExpr flowSum =cplex.prod(x[0][0][0],0);
							IloLinearNumExpr capReq = cplex.linearNumExpr();
							capReq.addTerm(1,x[k][i][j]);
							
								flowSum = cplex.sum(cplex.prod(f[k][i][j],1), 
										cplex.prod(f[k][j][i],1));
										
							
							cplex.addGe(flowSum, capReq);
						}
					}
				}

				
				//Constraint 13
				for (int k1=0;k1<reqLinksNum;k1++){
					for (int k2=0;k2<reqLinksNum;k2++){
						for (int i=0;i<reqNodeNum; i++){
							for (int j=reqNodeNum;j<augGraph; j++){
									IloLinearNumExpr capReq1 = cplex.linearNumExpr();
									IloLinearNumExpr capReq2 = cplex.linearNumExpr();
										capReq1.addTerm(1,x[k1][i][j]);
										capReq2.addTerm(1,x[k2][i][j]);
										cplex.addEq(capReq1,capReq2);
							}
						}
					}
				}
				
				
				/////////////////////////////////////////////
				//find the origins of the virtual link//////
				///////////////////////////////////////////
				int[] o=new int[reqLinksNum];
				int[] d=new int[reqLinksNum];
				for (Link link: req.getGraph().getEdges()){
					Pair<Node> pair = req.getGraph().getEndpoints(link);
					o[link.getId()]=pair.getFirst().getId();
					d[link.getId()]=pair.getSecond().getId();
				}

				
		

				//flow reserv 1
				for (Link link:req.getGraph().getEdges()){
					for (int i=0;i<augGraph; i++){ 
						IloLinearNumExpr capReq = cplex.linearNumExpr();
						if (i!=o[link.getId()]&&i!=d[link.getId()]){
							for (int j=0;j<augGraph; j++){
								capReq.addTerm(1,f[link.getId()][i][j]);
								capReq.addTerm(-1,f[link.getId()][j][i]);
							}
						cplex.addEq(capReq,0);
						}
					}
				}

				//flow reserv2
				double[][] tmpReqLinks = getCapTable(req.getGraph());

				for (Link link:req.getGraph().getEdges()){
					IloLinearNumExpr capReq = cplex.linearNumExpr();
					for (int w=0;w<augGraph;w++){
						capReq.addTerm(1,f[link.getId()][o[link.getId()]][w]);
						capReq.addTerm(-1,f[link.getId()][w][o[link.getId()]]);
					}
					double reCap = 0;
					reCap=link.getBandwidth();
					cplex.addEq(capReq,reCap); 
				}

				//flow reserv3
				for (Link link: req.getGraph().getEdges()){
					IloLinearNumExpr capReq = cplex.linearNumExpr();
					for (int w=0;w<augGraph;w++){
						capReq.addTerm(1,f[link.getId()][d[link.getId()]][w]);
						capReq.addTerm(-1,f[link.getId()][w][d[link.getId()]]);
					}
					double reCap = 0;
					reCap=-link.getBandwidth();
					cplex.addEq(capReq,reCap); 
				}
					
				//System.out.println(cplex.toString());
				//Solve the Model
				if ( cplex.solve() ==false ) {
					denial++;
					denials1++;
					denied=true;
					System.out.println("denial A "  +denial);
					cplex.end();

				}else {
					
					System.out.println("###################################");
					double[][][] xVar =new double [reqLinksNum][augGraph][augGraph];
					for (int k=0;k<reqLinksNum;k++){
						for (int i=0;i<augGraph;i++){
							xVar[k][i] = cplex.getValues(x[k][i]);
						}
					}
					
					double[][][] fVar =new double [reqLinksNum][augGraph][augGraph];
					for (int k=0;k<reqLinksNum;k++){
						for (int i=0;i<augGraph;i++){
							fVar[k][i] = cplex.getValues(f[k][i]);
						}
					}

					cplex.end();
					
					//Rounding of the linear solution
					NodeMapping = randomNodeMapping(tmpSubNodesList, this.substrate, tmpReqNodes,reqLinksNum, xVar, fVar );

					if (NodeMapping==null){
						  denial++;
					      denied=true;
					}
					else {
					tmpSubNodesList = 	(ArrayList<Node>) getNodes(substrateCopy.getGraph());
					Collections.sort(tmpSubNodesList,new NodeComparator());
					
					
					//create f continuous variable, with bounds lb and ub
					//////////////////////////////////////////////////////////////////
					IloNumVar[][][] f_mcf = new IloNumVar[reqLinksNum][][];
					for (int k=0;k<reqLinksNum;k++){
						f_mcf[k]=new IloNumVar[initSubNodeNum][];
						for (int i=0;i<initSubNodeNum;i++){
							f_mcf[k][i]=cplex1.numVarArray(initSubNodeNum, 0, Double.MAX_VALUE);
						}
					}
					///////////////////////////////////////////////////////////////////
					IloLinearNumExpr flows_mcf = cplex1.linearNumExpr();

					for (int i=0;i<initSubNodeNum; i++){ 
						for (int j=0;j<initSubNodeNum; j++){
							for (int k =0; k<reqLinksNum; k++) {
								flows_mcf.addTerm(1.0, f_mcf[k][i][j]);
							}
						}
					}
					
					
					
					//create objective minimization

					cplex1.addMinimize(flows_mcf);
					
					//cplex1.addMinimize(flows_mcf);

					int[] o2=new int[reqLinksNum];
					int[] d2=new int[reqLinksNum];
	


					for (int i=0; i<o.length;i++){
						if (NodeMapping.containsKey(tmpReqNodesList.get(o[i]))){
							o2[i]=NodeMapping.get(tmpReqNodesList.get(o[i])).getId()-reqNodeNum;
						}
					}
					for (int i=0; i<d.length;i++){
						if (NodeMapping.containsKey(tmpReqNodesList.get(d[i]))){
							d2[i]=NodeMapping.get(tmpReqNodesList.get(d[i])).getId()-reqNodeNum;
						}
					}
			
					
					//flow reservation1
					for (int k=0;k<reqLinksNum;k++){
						for (int i=0;i<initSubNodeNum;i++){
							IloLinearNumExpr capReq = cplex1.linearNumExpr();
							if ((o2[k]!=i)&&(d2[k]!=i)){
								for (int j=0;j<initSubNodeNum;j++){
									capReq.addTerm(1,f_mcf[k][i][j]);
									capReq.addTerm(-1,f_mcf[k][j][i]);
								}
							}
							cplex1.addEq(capReq,0);
						}
					}
					

				//flow reservation2
				for(int k=0;k<reqLinksNum;k++){
					IloLinearNumExpr capReq = cplex1.linearNumExpr();
					for (int i=0;i<initSubNodeNum;i++){
						capReq.addTerm(1,f_mcf[k][o2[k]][i]);
						capReq.addTerm(-1,f_mcf[k][i][o2[k]]);
					}
					double reCap = 0;
					reCap=tmpReqLinks[o[k]][d[k]];
					cplex1.addEq(capReq,reCap);
				}

				//flow reservation3
				for(int k=0;k<reqLinksNum;k++){
					IloLinearNumExpr capReq = cplex1.linearNumExpr();
					for (int i=0;i<initSubNodeNum;i++){
						capReq.addTerm(1,f_mcf[k][d2[k]][i]);
						capReq.addTerm(-1,f_mcf[k][i][d2[k]]);
					}
					double reCap = 0;
					reCap=-tmpReqLinks[o[k]][d[k]];
					cplex1.addEq(capReq,reCap);
				}
		
					//Link constraint
					for (int i=0;i<initSubNodeNum; i++){
						for (int j=0;j<initSubNodeNum; j++){
							IloNumExpr expr1 =cplex1.prod(f_mcf[0][0][0],0);
							IloNumExpr flowSum =cplex1.prod(f_mcf[0][0][0],0);
							for (int k=0;k<reqLinksNum; k++){
								flowSum = cplex1.sum(cplex1.prod(f_mcf[k][i][j],1), 
										cplex1.prod(f_mcf[k][j][i],1));
								expr1 =cplex1.sum(expr1,flowSum);
							}
							double capacity =augCapTable[reqNodeNum+i][reqNodeNum+j];
							cplex1.addLe(expr1,capacity);
						}
					}

					//System.out.println(cplex1.toString());
					if ( cplex1.solve() ) {	

					
						//Update the residual bandwidth

						for (int k=0;k<reqLinksNum;k++){
							for (int i=0;i<initSubNodeNum;i++){
								for (int j=0;j<initSubNodeNum;j++){
									fi[k][i][j]=cplex1.getValue(f_mcf[k][i][j]);
								}
							}
						}
						
	
					}///cplex1
					else{
						denial++;
						denials2++;
						System.out.println("denial C "  +denial);
						denied=true;
					}
					}//end of the NodeMapping != null
				}//end of the cplex.solve=true
				

					
				cplex1.end();

				// create model and solve it
				} catch (IloException e) {
				System.err.println("Concert exception caught: " + e);
				}

				ResourceMapping resMap=req.getRMap();
				
				if (NodeMapping!=null){
					@SuppressWarnings("rawtypes")
					Collection c = NodeMapping.entrySet();
					@SuppressWarnings("rawtypes")
					Iterator itr = c.iterator();
				
					while(itr.hasNext()){
						@SuppressWarnings("rawtypes")
						Map.Entry entry = (Map.Entry)itr.next();
						((Node)entry.getValue()).setId(((Node)entry.getValue()).getId()-req.getGraph().getVertexCount());
					}	
				}

			
							
				if (!denied){
					resMap.setNodeMapping(NodeMapping);
					req.getRMap().setLinkMapping(fi);
					req.getRMap().reserveNodes(this.substrate);
					req.getRMap().reserveLinks(this.substrate);
					req.getRMap().accepted();
	
				}else{
					req.getRMap().denied();
				}
				req.setRMap(resMap);
				
				///Take results
				Results results=new Results(this.substrate);
				double cost=results.Cost_Embedding(fi, req);
				double avgHops=results.avgHops(fi);
				retres[req_counter][0]=denial;
				retres[req_counter][1]=cost;
				retres[req_counter][2]=avgHops;
				retres[req_counter][3]=algo_denials;
				retres[req_counter][4]=denials1;


				if (denial==0){
					if (req.getProv()=="soft") retres[req_counter][7]=1; //soft provisioningn counter
					else retres[req_counter][8]=1; //hard provisioning counter
				}

				req_counter++;
		}
		return retres; 
	}

	
	
	///////////////////////////////////////////////////////////////////////////////////////////////////
	
	public Collection<Node> getNodes(Graph<Node,Link> t) {
		ArrayList<Node> reqNodes =new ArrayList<Node>();
		@SuppressWarnings("unused")
		Collection<Link> edges =  t.getEdges();
		
		for (Node x: t.getVertices())
			reqNodes.add(x);


		return reqNodes;
	}
	
	public double[][] getCapTable(Graph<Node,Link> t) {
		int num = t.getVertexCount();
		//Map<Pair<Node>, Double> table = new LinkedHashMap<Pair<Node>, Double>();
		Collection<Link> edges =  t.getEdges();
		
		double[][] tab =  new double[num][num];
		
		for (Link current : edges){
			Pair<Node> currentNodes =t.getEndpoints(current);
			
			int node1 = currentNodes.getFirst().getId();
			int node2= currentNodes.getSecond().getId();
			
			double cap = current.getBandwidth();
			tab[node1][node2]=cap;
			tab[node2][node1]=cap;
			//table.put(currentNodes, cap);
		}
		return tab;
	}
	
	public double[][] getBandTable(Graph<Node,Link> t) {
		int num = t.getVertexCount();
		//Map<Pair<Node>, Double> table = new LinkedHashMap<Pair<Node>, Double>();
		Collection<Link> edges =  t.getEdges();
		double[][] tab =  new double[num][num];
		
		for (Link current : edges){
			Pair<Node> currentNodes =t.getEndpoints(current);
			int node1 = currentNodes.getFirst().getId();
			int node2= currentNodes.getSecond().getId();
			double cap = current.getBandwidth();
			tab[node1][node2]=cap;
			tab[node2][node1]=-cap;
			//table.put(currentNodes, cap);
		}
		
		
		
		return tab;
	}
	
		
	
	public LinkedHashMap<String, Integer> getType (@SuppressWarnings("rawtypes") ArrayList list) {
		LinkedHashMap<String, Integer>  tmp = new LinkedHashMap<String, Integer> ();


	    for (int i=0;i<list.size();i++){   
		     if ((list.get(i) instanceof Server) ) {
		    	 	if (tmp.containsKey("Server")==false)
		    	 		tmp.put("Server", 1);
		    	 	else 	    	 
		    	 		tmp.put("Server", tmp.get("Server")+1);
		    	 }
		     else  if ((list.get(i) instanceof SubstrateRouter) ) {
		    	 	if (tmp.containsKey("SubstrateRouter")==false)
		    	 		tmp.put("SubstrateRouter", 1);
		    	 	else 	    	 
		    	 		tmp.put("SubstrateRouter", tmp.get("SubstrateRouter")+1);
		    	 }
		     else if ((list.get(i) instanceof SubstrateSwitch) ) {
		    	 	if (tmp.containsKey("SubstrateSwitch")==false)
		    	 		tmp.put("SubstrateSwitch", 1);
		    	 	else 	    	 
		    	 		tmp.put("SubstrateSwitch", tmp.get("SubstrateSwitch")+1);
		    	 }
		     else if ((list.get(i) instanceof VirtualMachine) ){
		    	 	if (tmp.containsKey("VirtualMachine")==false)
		    	 		tmp.put("VirtualMachine", 1);
		    	 	else 	    	 
		    	 		tmp.put("VirtualMachine", tmp.get("VirtualMachine")+1);
		    	 }
		    	 
		     else if ((list.get(i) instanceof RequestRouter) ){
		    	 	if (tmp.containsKey("RequestRouter")==false)
		    	 		tmp.put("RequestRouter", 1);
		    	 	else 	    	 
		    	 		tmp.put("RequestRouter", tmp.get("RequestRouter")+1);
		    	 }
		     else if ((list.get(i) instanceof RequestSwitch) ) {
		    	 	if (tmp.containsKey("RequestSwitch")==false)
		    	 		tmp.put("RequestSwitch", 1);
		    	 	else 	    	 
		    	 		tmp.put("RequestSwitch", tmp.get("RequestSwitch")+1);
		    	 }


		   }
		    
	    return tmp;

		}

		public ArrayList<ArrayList<Integer>> createArray (@SuppressWarnings("rawtypes") ArrayList list) {
			 ArrayList<ArrayList<Integer>>  tmp = new  ArrayList<ArrayList<Integer>>();
			 
			 	 
			 for (int i=0;i <MAX_TYPES;i++)
				 tmp.add(new ArrayList<Integer>());
			 


			  for (int i=0;i<list.size();i++){   
				     if ((list.get(i) instanceof Server) ) {
				    	 tmp.get(0).add(i);
				    	 }
				     else  if ((list.get(i) instanceof SubstrateRouter) ) {
				    	 	tmp.get(1).add(i);
				    	 }
				     else if ((list.get(i) instanceof SubstrateSwitch) ) {
				    	 tmp.get(2).add(i);
				    	 }
				     else if ((list.get(i) instanceof VirtualMachine) ){
				    	 	tmp.get(0).add(i);
				    	 	}
				    	 
				     else if ((list.get(i) instanceof RequestRouter) ){
				    	 	tmp.get(1).add(i);
				    	 	}
				     else if ((list.get(i) instanceof RequestSwitch) ) {
				    	 	tmp.get(2).add(i);
				    	 	}


				   }
					
				return tmp;

			}

		public int[][] getCapacities (@SuppressWarnings("rawtypes") ArrayList list){
			int[][] tmp =  new int[3][list.size()];

			for (int i=0;i<list.size();i++){  
			if ((list.get(i) instanceof VirtualMachine) ){
				tmp[0][i]= ((VirtualMachine)list.get(i)).getCpu();
				tmp[1][i]=((VirtualMachine)list.get(i)).getMemory();
				tmp[2][i]=((VirtualMachine)list.get(i)).getDiskSpace();
			}
			if ((list.get(i) instanceof RequestRouter) ){
				tmp[0][i]=1;
				}
			if ((list.get(i) instanceof RequestSwitch) ){
				tmp[0][i] = 1;
				//tmp[0][i]=((RequestSwitch)list.get(i)).getCpu();
				//tmp[1][i]=((RequestSwitch)list.get(i)).getMemory();
				}
			if ((list.get(i) instanceof Server) ){
				tmp[0][i]= ((Server)list.get(i)).getCpu();
				tmp[1][i]=((Server)list.get(i)).getMemory();
				tmp[2][i]=((Server)list.get(i)).getDiskSpace();
			}
			if ((list.get(i) instanceof SubstrateRouter) ){
				tmp[0][i]=((SubstrateRouter)list.get(i)).getLogicalInstances();
				}
			if ((list.get(i) instanceof SubstrateSwitch) ){
				tmp[0][i]=((SubstrateSwitch)list.get(i)).getLogicalInstances();
				//tmp[0][i]=((SubstrateSwitch)list.get(i)).getCpu();
				//tmp[1][i]=((SubstrateSwitch)list.get(i)).getMemory();
				}

			}

			return tmp;
		}
		

	

		
	public 	LinkedHashMap<Node, Node> determNodeMapping(ArrayList<Node> substrate, Substrate realSub, Collection<Node> request, int reqLinksNum, double[][][] xVar, double[][][] fVar){
			LinkedHashMap<Node, Node> phi = new LinkedHashMap<Node, Node>(); // request-real
			//augmented substrate
			double[] fai=new double[realSub.getGraph().getVertexCount()];
			double[] pai=new double[realSub.getGraph().getVertexCount()];
			boolean ctrl=true;
			for (Node node:request){
				double max=0;
				int index_max=0;
				Node selected=null;
				for (Node subNode:substrate){
					if (node.getType().equalsIgnoreCase(subNode.getType())){
						for (int k=0;k<reqLinksNum;k++){
								pai[subNode.getId()-request.size()]=pai[subNode.getId()-request.size()]+(fVar[k][node.getId()][subNode.getId()]+fVar[k][subNode.getId()][node.getId()])*xVar[k][node.getId()][subNode.getId()];
						}
						if (pai[subNode.getId()-request.size()]>max&&fai[subNode.getId()-request.size()]==0){
							max=pai[subNode.getId()-request.size()];
							index_max=subNode.getId()-request.size();
							selected=subNode;
						}
					}
				}
				if (max!=0){
					fai[index_max]=1;
					phi.put(node, selected);
				}
				else{
					System.out.println("node can not be mapped");
					ctrl=false;
				}
			}
			if (ctrl==true)
				return phi;
			else
				return null;
		}
	
	
	
	public 	LinkedHashMap<Node, Node> randomNodeMapping(ArrayList<Node> substrate, Substrate realSub, Collection<Node> request, int reqLinksNum, double[][][] xVar, double[][][] fVar){
		
		LinkedHashMap<Node, Node> phi = new LinkedHashMap<Node, Node>(); // request-real
		//augmented substrate
		double[] fai=new double[realSub.getGraph().getVertexCount()];
		double[] pai=new double[realSub.getGraph().getVertexCount()];
		boolean ctrl=true;
		for (Node node:request){
			double psum=0;
			Node selected=null;
			
			for (Node subNode:substrate){
				if ((node.getType().equalsIgnoreCase(subNode.getType()))&&(fai[subNode.getId()-request.size()]==0)){
					for (int k=0;k<reqLinksNum;k++){
						pai[subNode.getId()-request.size()]+=(fVar[k][node.getId()][subNode.getId()]+fVar[k][subNode.getId()][node.getId()])*xVar[k][node.getId()][subNode.getId()];
					}
					
				}
			}
			
			for (Node snode:substrate){
				if ((node.getType().equalsIgnoreCase(snode.getType()))&&(fai[snode.getId()-request.size()]==0)){
					psum+=pai[snode.getId()-request.size()];
				}
			}
			
			double prob=Math.random();
			double cdf=0;
			for (Node snode:substrate){
				if ((node.getType().equalsIgnoreCase(snode.getType()))&&(fai[snode.getId()-request.size()]==0)){
					pai[snode.getId()-request.size()]=pai[snode.getId()-request.size()]/psum;
					cdf+=pai[snode.getId()-request.size()];
					if (cdf>prob){
						fai[snode.getId()-request.size()]=1;
						selected=snode;
						phi.put(node, snode);
						break;
					}
				}
			}
			
			
			if (selected==null){
				System.out.println("node can not be mapped");
				ctrl=false;
			}
		}
		if (ctrl==true)
			return phi;
		else
			return null;
	
}
	
	
		
	}
	
