package main;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import cern.jet.random.Exponential;
import cern.jet.random.engine.DRand;
import cern.jet.random.engine.RandomEngine;

import edu.uci.ics.jung.graph.SparseMultigraph;
import edu.uci.ics.jung.graph.UndirectedGraph;


import gui.SimulatorConstants;

import model.Algorithm;
import model.NetworkGraph;
import model.Request;
import model.RequestLinkFactory;
import model.RequestNodeFactory;
import model.ResourceMapping;
import model.Simulation;
import model.Substrate;
import model.SubstrateLinkFactory;
import model.SubstrateNodeFactory;
import output.Results;
import model.components.Link;
import model.components.Node;
import model.components.Server;
import model.components.SubstrateRouter;
import model.components.VirtualMachine;

import java.util.HashSet;
import org.apache.commons.collections15.Factory;

import edu.uci.ics.jung.algorithms.cluster.WeakComponentClusterer;
import edu.uci.ics.jung.algorithms.filters.FilterUtils;
import edu.uci.ics.jung.algorithms.generators.random.BarabasiAlbertGenerator;
import edu.uci.ics.jung.algorithms.generators.random.ErdosRenyiGenerator;
import edu.uci.ics.jung.graph.Graph;


import java.io.*;
import jxl.*;
import java.util.*;
import jxl.Workbook;
import jxl.write.Number;

import jxl.write.*;

public class MainWithoutGUIfull {

	public static void main(String[] args) {
		
		//Number of experiments to execute
	    int experiments=1;
	   

		for (int i =0; i<experiments; i++){
			
			//Create the substrate network
			Substrate substrate=new Substrate("sub");
			substrate= createSubstrateGraph(substrate);
			
			//Create a bulk of requests
			List<Request> request_tab = new ArrayList<Request>();
			request_tab =randomRequestGeneration();
    
			//Run the NCM algorithm
			Algorithm algorithm = new Algorithm("NCM");
			Simulation simulation = new Simulation(substrate, request_tab, algorithm);
			launchLaunchSimulation(simulation, i);

			//Clean the requests from mappings
			for (Request x: request_tab){
				x.setRMap(new ResourceMapping(x));
			}
			
			//Run the G-MCF algorithm
			algorithm = new Algorithm("GMCF");
			simulation = new Simulation(substrate, request_tab, algorithm);
			launchLaunchSimulation(simulation, i);
			
			//Clean the requests from mappings
			for (Request x: request_tab){
		    x.setRMap(new ResourceMapping(x));
			}
			
			//Run the G-SP algorithm
			algorithm = new Algorithm("GSP");
			simulation = new Simulation(substrate, request_tab, algorithm);
			launchLaunchSimulation(simulation, i);

		}
	}

	@SuppressWarnings("unused")
	private static void launchLaunchSimulation(Simulation simulation, int inD) {
		
		  try
		    {
			  //Extract the result in a xls file
			  Calendar calendar = Calendar.getInstance();
			  java.util.Date now = calendar.getTime();
			  java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(now.getTime());
			  String name =  currentTimestamp.toString();
			  String[] out = name.split(" ");
			  String[] out1 = out[1].split(":");
		      String filename = "input"+out[0]+"_"+out1[0]+"-"+out1[1]+"_"+ simulation.getAlgorithm().getId()+"_"+inD+".xls";
		      WorkbookSettings ws = new WorkbookSettings();
		      ws.setLocale(new Locale("en", "EN"));
		      WritableWorkbook workbook =  Workbook.createWorkbook(new File(filename), ws);
		      WritableSheet s = workbook.createSheet("Sheet1", 0);

					    /* Format the Font */
				    WritableFont wf = new WritableFont(WritableFont.ARIAL, 
				      10, WritableFont.BOLD);
				    WritableCellFormat cf = new WritableCellFormat(wf);
				    cf.setWrap(true);
				      Label l = new Label(0,0,"Time",cf);
				      s.addCell(l);
				      l= new Label(1,0,"Acceptance",cf);
				      s.addCell(l);
				      l=new Label(2,0,"Revenue",cf);
				      s.addCell(l);
				      l=new Label(3,0,"Cost",cf);
				      s.addCell(l);
				      l=new Label (4,0,"Hops", cf);
				      s.addCell(l);
				      l=new Label (5,0,"NServer_Util_Cpu",cf);
				      s.addCell(l);
				      l=new Label (6,0,"NServer_Util_Memory",cf);
				      s.addCell(l);
				      l=new Label (7,0,"NServer_Util_DiskSpace",cf);
				      s.addCell(l);
				      l=new Label (8,0,"NRouter_Util",cf);
				      s.addCell(l);
				      l=new Label (9,0,"L_Util",cf);
				      s.addCell(l);
				      l=new Label (10,0,"StressNS",cf);
				      s.addCell(l);
				      l=new Label (11,0,"StressR",cf);
				      s.addCell(l);
				      l=new Label (12,0,"StressL",cf);
				      s.addCell(l);
				      l=new Label (13,0,"Soft",cf);
				      s.addCell(l);
				      l=new Label (14,0,"Hard",cf);
				      s.addCell(l);
				      l=new Label (15,0,"2",cf);
				      s.addCell(l);
				      l=new Label (16,0,"3",cf);
				      s.addCell(l);
				      l=new Label (17,0,"4",cf);
				      s.addCell(l);
				      l=new Label (18,0,"5",cf);
				      s.addCell(l);
				      l=new Label (19,0,"6",cf);
				      s.addCell(l);
				      l=new Label (20,0,"7",cf);
				      s.addCell(l);
				      l=new Label (21,0,"8",cf);
				      s.addCell(l);
				      l=new Label (22,0,"9",cf);
				      s.addCell(l);
				      l=new Label (23,0,"10",cf);
				      s.addCell(l);
				      l=new Label (24,0,"Cost2",cf);
				      s.addCell(l);
				      l=new Label (25,0,"Cost3",cf);
				      s.addCell(l);
				      l=new Label (26,0,"Cost4",cf);
				      s.addCell(l);
				      l=new Label (27,0,"Cost5",cf);
				      s.addCell(l);
				      l=new Label (28,0,"Cost6",cf);
				      s.addCell(l);
				      l=new Label (29,0,"Cost7",cf);
				      s.addCell(l);
				      l=new Label (30,0,"Cost8",cf);
				      s.addCell(l);
				      l=new Label (31,0,"Cost9",cf);
				      s.addCell(l);
				      l=new Label (32,0,"Cost10",cf);
				      s.addCell(l);
				      l=new Label (34, 0, "Av Costs");
				      s.addCell(l);
				    
		
		int denials = 0;
		double cost=0;
	    double revenue=0;
	    double avgHops =0;
	    double algo_den=0;
	    double[] sizes=new double[9];
	    double[] costs=new double[9];
	    int totallinks=0;
	    int current_request=0;
	    int soft=0;
	    int hard=0;
	    
	    int simulationTime = (int)simulation.getEndDate();
		List<Request> requests = simulation.getRequests();
		Substrate substrate = simulation.getSubstrate();
		Algorithm algorithm = simulation.getAlgorithm();
		List<Request> endingRequests;
	    List<Request> startingRequests;
	    algorithm.addSubstrate(substrate);
	    
	    //Find the initial Node and Link capacity of the substrate to use it in
	    //the node utilization and link utilization result.
	    //Find also the number of Substrate Servers, Switch, Routers
	    int servers=0;
	    int routers=0;
	    int switches=0;
	    double[] n_initial_Cpu=new double[substrate.getGraph().getVertexCount()];
	    double[] n_initial_Memory=new double[substrate.getGraph().getVertexCount()];
	    double[] n_initial_DiskSpace=new double[substrate.getGraph().getVertexCount()];
	    for (Node x: substrate.getGraph().getVertices()){
	    	if (x instanceof Server){
	    		n_initial_Cpu[x.getId()]=x.getCpu();
	    		n_initial_Memory[x.getId()]=x.getMemory();
	    		n_initial_DiskSpace[x.getId()]=((Server) x).getDiskSpace();
	    		servers++;
	    	}
	    	else if (x instanceof SubstrateRouter){
	    		n_initial_Cpu[x.getId()]=x.getCpu();
	    		n_initial_Memory[x.getId()]=x.getMemory();
	    		routers++;
	    	}
	    }
	    
	    double[] l_initial=new double[substrate.getGraph().getEdgeCount()];
	    
	    for (Link current: substrate.getGraph().getEdges()){
	    	l_initial[current.getId()]=current.getBandwidth();
	    }
/////////////////////////////////////////////////////////////////////
	    
	    Results results=new Results(substrate);
	    int counter2=0;
	    //substrate.print();
	        for (int i=0; i<simulationTime; i++) {
	        
	            // Release ended simulations in the moment "i"
	            endingRequests = simulation.getEndingRequests(i);
	            simulation.releaseRequests(endingRequests);
	            // Allocate arriving simulations in the moment "i"
	            startingRequests = simulation.getStartingRequests(i);
	            algorithm.addRequests(startingRequests);
      			double[][] retres=algorithm.runAlgorithm();
      			
      			for (int ind=0; ind<startingRequests.size();ind++){
      				denials += (int)retres[ind][0];
      				cost+=retres[ind][1];
      				costs[startingRequests.get(ind).getGraph().getVertexCount()-2]+=retres[ind][1];
      				avgHops +=retres[ind][2];
      				algo_den +=retres[ind][3];
      				soft+=retres[ind][7];
      				hard+=retres[ind][8];
       			}
      			
      			for (Request req:startingRequests){
      				if (!req.getRMap().isDenied()){
      					revenue=revenue+results.Generate_Revenue(req);
      					sizes[req.getGraph().getVertexCount()-2]++;
      				}
          				current_request++;
          				
          				totallinks+=req.getGraph().getEdgeCount();
          				
          			
          				System.out.println("denials: "+denials);
          				System.out.println("current request: "+current_request);
      			}
    
          	//Take results every 100 time units
	            if ((i%100)==0){
	            	counter2++;
	            	double[] nServer1_util=results.Node_utilization_Server_Cpu(n_initial_Cpu);
	            	double[] nServer2_util=results.Node_utilization_Server_Memory(n_initial_Memory);
	            	double[] nServer3_util=results.Node_utilization_Server_DiskSpace(n_initial_DiskSpace);
	            	double[] nRouter1_util=results.Node_utilization_Router();
	            	double[] l_util=results.Link_utilization(l_initial);
	            	double nServer1_average=0;
	            	double nServer2_average=0;
	            	double nServer3_average=0;
	            	double nRouter1_average=0;
	            	double l_average=0;
	            	for (int j=0;j<substrate.getGraph().getVertexCount();j++){
	            		nServer1_average += nServer1_util[j];
	            		nServer2_average += nServer2_util[j];
	            		nServer3_average += nServer3_util[j];
	            		nRouter1_average += nRouter1_util[j];
	            	}
	            	for (int j=0;j<substrate.getGraph().getEdgeCount();j++){
	            		if (l_util[j]<1000000000)
	            		l_average=l_average+l_util[j];
	            	}
	            	
	            	Number n = new Number(0,counter2,i);
	                s.addCell(n);
	                Number n1 = new Number(1,counter2,results.Acceptance_Ratio(denials, current_request));
	                s.addCell(n1);
	                Number n2 = new Number(2,counter2,revenue/i);
	                s.addCell(n2);
	                Number n3 = new Number(3,counter2,cost/(current_request-denials));
	                s.addCell(n3);
	            	Number n4 = new Number(4,counter2,avgHops/(current_request-denials));
	                s.addCell(n4);
	                Number n5 = new Number(5,counter2,(nServer1_average/servers));
	                s.addCell(n5);
	                Number n6 = new Number(6,counter2,(nServer2_average/servers));
	                s.addCell(n6);
	                Number n7 = new Number(7,counter2,(nServer3_average/servers));
	                s.addCell(n7);
	                Number n8 = new Number(8,counter2,(nRouter1_average/routers));
	                s.addCell(n8);
	                Number n9 = new Number(9,counter2,(l_average/substrate.getGraph().getEdgeCount()));
	            	s.addCell(n9);

	               Number n13 = new Number(13, counter2, soft);
	               s.addCell(n13);
	               Number n14 = new Number(14, counter2, hard);
	               s.addCell(n14);
	              
	               
	            }
	        }
	        
	        for (int i=0;i<9;i++){
	        	Number n15 = new Number (15+i, 1, sizes[i] );
	        	s.addCell(n15);
	        	Number n24 = new Number (24+i, 1, costs[i]);
	        	s.addCell(n24);
	        }
	        
	        
	        for (int i=0;i<9;i++){
	        	Number n34 = new Number (34,1+i, (double) costs[i]/sizes[i]);
	        	s.addCell(n34);
	        }
	        
			System.out.println();
			//System.out.println("simulation time: "+simulationTime);
			System.out.println("denials: "+denials);
			System.out.println("current request: "+requests.size());
			
			
      	
		    workbook.write();
		    workbook.close(); 
		    }
		    catch (IOException e)
		    {
		      e.printStackTrace();
		    }
		    catch (WriteException e)
		    {
		      e.printStackTrace();
		    }
		
	}
	
	


	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static List<Request> randomRequestGeneration() {
		final String prefix ="req";
		 final int numRequests = 1000;
		 final int minNodes =2;
		 final int maxNodes=10;
		 final int minLinks=1;
		 final int maxLinks=1;
		 final String timeDistribution = SimulatorConstants.POISSON_DISTRIBUTION;
		 final String linkConnectivity = SimulatorConstants.PERCENTAGE_CONNECTIVITY;
		 final double linkProbability=0.5;
		 final int fixStart=0;
		 final int uniformMin=0;
		 final int uniformMax=0;
		 final int normalMean=0;
		 final int normalVariance=0;
		RequestNodeFactory nodeFactory;
		RequestLinkFactory linkFactory;
		final List<Request> requests = new ArrayList<Request>();

	        	int startDate = 0;
	        	int lifetime = 0;
	        	int sum=0;

	        	Exponential exp_arr = null;
	        	Exponential exp = null;
	        	if (timeDistribution.equals(SimulatorConstants.POISSON_DISTRIBUTION)) {
		          	RandomEngine engine = new DRand();
		            exp_arr = new Exponential(0.01,engine);
		        	exp= new Exponential(0.001,engine);
	        	}


				for (int i=0; i<numRequests; i++) {
					Request request = new Request(prefix+i);			
					
					// Random num of nodes inside range (minNodes-maxNodes)
					int numNodes = minNodes + (int)(Math.random()*((maxNodes - minNodes) + 1));
					// Random num of links inside range (minLinks-maxLinks)
					int numLinks = minLinks + (int)(Math.random()*((maxLinks - minLinks) + 1));
					//lifetime of the request
					lifetime = exp.nextInt();
					
					SparseMultigraph<Node, Link> g = null;
					
					nodeFactory = new RequestNodeFactory();
					linkFactory = new RequestLinkFactory();
					
					if (linkConnectivity.equals(SimulatorConstants.LINK_PER_NODE_CONNECTIVITY)) {
						//Random Graph Generation
						Factory<Graph<Node, Link>> graphFactory = new Factory<Graph<Node, Link>>() {
							public UndirectedGraph<Node, Link> create() {
								return new NetworkGraph();
							}
						};
						//Barabasi-Albert generation
						BarabasiAlbertGenerator<Node, Link> randomGraph = new BarabasiAlbertGenerator<Node, Link>(graphFactory, nodeFactory, linkFactory, 1, numLinks, new HashSet<Node>());
						randomGraph.evolveGraph(numNodes-1);
						g = (SparseMultigraph<Node, Link>) randomGraph.create();
					}
					else if (linkConnectivity.equals(SimulatorConstants.PERCENTAGE_CONNECTIVITY)) {
						
						//Random Graph Generation
						Factory<UndirectedGraph<Node, Link>> graphFactory = new Factory<UndirectedGraph<Node, Link>>() {
							public UndirectedGraph<Node, Link> create() {
								return new NetworkGraph();
							}
						};
						//ErdosRenyiGenerator generation
						ErdosRenyiGenerator<Node, Link> randomGraph = new ErdosRenyiGenerator<Node, Link>(graphFactory, nodeFactory, linkFactory, numNodes, linkProbability);
						g = (SparseMultigraph<Node, Link>) randomGraph.create();
						//Remove unconnected nodes
						((NetworkGraph) g).removeUnconnectedNodes();
						// TODO remove disconnected graphs
						WeakComponentClusterer<Node, Link> wcc = new WeakComponentClusterer<Node, Link>();
						Set<Set<Node>> nodeSets = wcc.transform(g);
						Collection<SparseMultigraph<Node, Link>> gs = FilterUtils.createAllInducedSubgraphs(nodeSets, g); 
						if (gs.size()>1) {
							Iterator itr = gs.iterator();
							g = (NetworkGraph)itr.next();
							while (itr.hasNext()) {
								SparseMultigraph<Node, Link> nextGraph = (SparseMultigraph<Node, Link>) itr.next();
						
								if (nextGraph.getVertexCount()>g.getVertexCount())
									g = (NetworkGraph)nextGraph;
							}
						}
						
						if (g.getVertexCount()>0){
							// Change id of nodes to consecutive int (0,1,2,3...)
							Iterator itr = g.getVertices().iterator();
							int id = 0;
							while (itr.hasNext()) {
								((Node) itr.next()).setId(id);
								id++;
							}
							// refresh nodeFactory's nodeCount
							nodeFactory.setNodeCount(id);
							// Change id of edges to consecutive int (0,1,2,3...)
							itr = g.getEdges().iterator();
							id = 0;
							while (itr.hasNext()) {
								((Link) itr.next()).setId(id);
								id++;
							}
							// refresh linkFactory's linkCount
							linkFactory.setLinkCount(id);
						}

					}
					
				if (g.getVertexCount()>0){
					
					request.setGraph(g);
					request.setNodeFactory(nodeFactory);
					request.setLinkFactory(linkFactory);
					
					// Random start/end dates
 
					// Duration of the request
					lifetime = exp.nextInt();
					// All requests start at month fixStart
					if (timeDistribution.equals(SimulatorConstants.FIXED_DISTRIBUTION)) {
						request.setStartDate(fixStart);
						request.setEndDate(fixStart+lifetime);
					}
					
					// Random: Uniform distribution
					if (timeDistribution.equals(SimulatorConstants.UNIFORM_DISTRIBUTION)) {
						startDate = uniformMin + (int)(Math.random()*((uniformMax - uniformMin) + 1));
						request.setStartDate(startDate);
						request.setEndDate(startDate+lifetime);
					}
					
					// Random: Normal distribution
					if (timeDistribution.equals(SimulatorConstants.NORMAL_DISTRIBUTION)) {
						Random random = new Random();
						startDate = (int) (normalMean + random.nextGaussian() * normalVariance);
						if (startDate<0)
							startDate*=-1;
						request.setStartDate(startDate);
						request.setEndDate(startDate+lifetime);
					}

					// Random: Poisson distribution
					if (timeDistribution.equals(SimulatorConstants.POISSON_DISTRIBUTION)) {
						startDate = exp_arr.nextInt();
						sum=sum+startDate;
						if (lifetime==0)
							lifetime=lifetime+1;
						request.setStartDate(sum);
						request.setEndDate(sum+lifetime);
					}
				
					// Domain
					// Not defined
					
					int prov=(int) (Math.random()*2);
					if (prov==0) request.setProv("soft");
					else request.setProv("hard");
					
					double percentage = 0.5;
					if (request.getProv()=="soft"){
						for (Node node:request.getGraph().getVertices()){
							if (node instanceof VirtualMachine){
							node.setCpu((int)Math.round(percentage*node.getCpu()));
							node.setMemory((int) Math.round(percentage*node.getMemory()));
							((VirtualMachine) node).setDiskSpace((int) Math.round(percentage*((VirtualMachine) node).getDiskSpace()));
							}
						}
			/*					for (Link link:req.getGraph().getEdges()){
							link.setBandwidth((int) Math.round(percentage*link.getBandwidth()));
						}*/
					}
					
					requests.add(request);
				}
				}

        return requests;
	}
	
	
 @SuppressWarnings({ "rawtypes", "unchecked" })
public static Substrate createSubstrateGraph(Substrate substrate){
		int numNodes = 50;
		double linkProbability = 0.5;	
		SparseMultigraph<Node, Link> g = null;	
		SubstrateNodeFactory nodeFactory = new SubstrateNodeFactory();
		SubstrateLinkFactory linkFactory = new SubstrateLinkFactory();
		
		//Random Graph Generation
		Factory<UndirectedGraph<Node, Link>> graphFactory = new Factory<UndirectedGraph<Node, Link>>() {
			public UndirectedGraph<Node, Link> create() {
				return new NetworkGraph();
			}
		};
			
			//ErdosRenyiGenerator generation
		ErdosRenyiGenerator<Node, Link> randomGraph = new ErdosRenyiGenerator<Node, Link>(graphFactory, nodeFactory, linkFactory, numNodes, linkProbability );
		g = (SparseMultigraph<Node, Link>) randomGraph.create();
		
			//Remove unconnected nodes
		((NetworkGraph) g).removeUnconnectedNodes();
			// TODO remove disconnected graphs
		WeakComponentClusterer<Node, Link> wcc = new WeakComponentClusterer<Node, Link>();
		Set<Set<Node>> nodeSets = wcc.transform(g);
		Collection<SparseMultigraph<Node, Link>> gs = FilterUtils.createAllInducedSubgraphs(nodeSets, g); 
		if (gs.size()>1) {
				Iterator itr = gs.iterator();
				g = (NetworkGraph)itr.next();
				while (itr.hasNext()) {
					SparseMultigraph<Node, Link> nextGraph = (SparseMultigraph<Node, Link>) itr.next();
			
					if (nextGraph.getVertexCount()>g.getVertexCount())
						g = (NetworkGraph)nextGraph;
				}
		}
			
		if (g.getVertexCount()>0){
				// Change id of nodes to consecutive int (0,1,2,3...)
				Iterator itr = g.getVertices().iterator();
				int id = 0;
				while (itr.hasNext()) {
					((Node) itr.next()).setId(id);
					id++;
				}
				// refresh nodeFactory's nodeCount
				nodeFactory.setNodeCount(id);
				// Change id of edges to consecutive int (0,1,2,3...)
				itr = g.getEdges().iterator();
				id = 0;
				while (itr.hasNext()) {
					((Link) itr.next()).setId(id);
					id++;
				}
				// refresh linkFactory's linkCount
				linkFactory.setLinkCount(id);
		}
		
		if  ((g.getVertexCount()==numNodes)){
			
			substrate.setGraph(g);
			substrate.setNodeFactory(nodeFactory);
			substrate.setLinkFactory(linkFactory);

			return substrate;
		}
		else
			createSubstrateGraph(substrate);
	
		return substrate;
 }
	
}